#!/bin/bash
#ESTE ES EL SCRIPT PARA CONFIGURAR SPAMSASSASSIN CON LO DOVECOT, POSTFIX E INSTALACION DE SUS RESPECTIVOS PLUGINS
#EL PRIMER PASO ES CREAR EL GRUPO DE USUARIOS
#CREAR EL GRUPO
function DetectUser(){
 if [ "$UID" -ne 0 ]
   export SUDO="sudo";
 else
   export SUDO="";
 fi;
}
DetectUser();
##Crear usuario spamd y grupo de usuario SPAM###
export HOME_FILE="/var/log/spamassassin"
function CreateUser(){
 $SUDO groupadd spamd
 $SUDO useradd -g spamd -s /bin/false -d $HOME_FILE spamd
 $SUDO mkdir $HOME_FILE;
 $SUDO chown spamd:spamd $HOME_FILE
}
###ACTUALIZAR CONFIGURACION DEL FICHERO DEFAULT###########3
export DEFAULT_FILE="/etc/default/spamassassin";
function Update_Defaults(){
    if [ -e "$DEFAULT_FILE" ];then
      var=$(grep -i "ENABLED=0|^#ENABLED" $DEFAULT_FILE);
      var2=$(grep -i "SAHOME=" $DEFAULT_FILE);
      var3=$(grep -i "^OPTIONS" $DEFAULT_FILE);
      var4=$(grep -i "^#CRON.CRON=0" $DEFAULT_FILE)
      [ "$var" ]&& $SUDO sed -i "s|ENABLED=0|ENABLED=1|g" $DEFAULT_FILE;
      [ -z "$var2" ]&& echo "SAHOME=$HOME_FILE" |$SUDO tee -a $DEFAULT_FILE;
      [ "$var3" ]&& $SUDO sed -i "s|OPTIONS|#OPTIONS|g" $DEFAULT_FILE && echo "OPTIONS='--create-prefs --max-children 5 --helper-home-dir --username spamd \
-H ${SAHOME} -s ${SAHOME}spamd.log'"| $SUDO tee -a $DEFAULT_FILE;                                                                                                   [ "$var4" ]&& $SUDO sed -i -"s|CRON|#CRON|g" $DEFAULT_FILE && $SUDO echo "CRON=1"|tee -a $DEFAULT_FILE;
   else 
      echo "No ha instalado adecuadamente el software, por favor vuela por medio de la paqueteria a reinstalar";
   fi;
}
Update_Defaults();
#Configuracion de los ficheros pertinentes al programa spam asesino
export LOCAL_FILE="/etc/spamassassin/local.cf";"
function Config_LocalCF(){
    if[ -e "$LOCAL_FILE" ];then
        var=$(grep -i "^#.rewrite_header Subject" $LOCAL_FILE);
        var2=$(grep -i "^#.required_score 4.0" $LOCAL_FILE);
        [ $var ]&& $SUDO sed -i "s|#rewrite_header Subject|rewrite_header Subject|g" $LOCAL_FILE;
        [ $var ]&& $SUDO sed -i "s|#required_score 5.0|required_score 4.0|g" $LOCAL_FILE;
    $SUDO echo pyzor_options --homedir /etc/mail/spamassassin/pyzor >>$LOCAL_FILE
    else
        echo "No ha instalado correctamente el software";
    
    fi;
    $SUDO service spamassassin restart;
}
Config_LocalCF();
####Integracion con el servicio de postfix###########
export POSTFIX_CONFIG_MASTER="/etc/postfix/master.cf"; #fichero maestro el cual sobre escribe las directrices del archivo principal o inicial de configuracion
function Integrate_Postfix(){
    if [ -e $POSTFIX_CONFIG_MASTER ];then
        $SUDO echo "spamassassin unix -     n       n       -       -       pipe
        user=spamd argv=/usr/bin/spamc -f -e  
        /usr/sbin/sendmail -oi -f ${sender} ${recipient}" |tee -a $POSTFIX_CONFIG_MASTER;
        $SUDO systemctl restart postfix;
        echo "Por favor editar en el archivo $POSTFIX_CONFIG_MASTER la siguiente linea:\n"
        echo "-o content_filter=spamassassin";
    else
        echo "Instale y configure Postfix o vuelvalo a configurar";
    fi;
}
Integrate_Postfix();
#Integracion con dovecot
export PLUGIN_DIR_DOVECOT="/etc/dovecot/conf.d/20-lmtp.conf"; #archivo de configuracion dovecto
export PLUGIN_DOVECOT_SIEVE="/etc/dovecot/conf.d/90-sieve.conf"
function Config_Plugin_Dovecot(){
    if [ -e "$PLUGIN_DIR_DOVECOT" ];then
        valu_exist=$(grep -i "protocol lmtp {" $PLUGIN_DIR_DOVECOT); #evalua la existencia de la ruta
        [ valu_exist ]&&text_replace="protocol lmtp {
  mail_plugins = $mail_plugins sieve
  postmaster_address = tinus@example.com
}"&& data2="protocol lmtp {
  # Space separated list of plugins to load (default is global mail_plugins).
  #mail_plugins = $mail_plugins
 }"&& $SUDO sed -i "s|$text_replace|$data2|g" $PLUGIN_DIR_DOVECOT
########Se sustitye el archivo###############################################################3
    [ -e "$PLUGIN_DOVECOT_SIEVE "]&& echo "Modifique el archivo $PLUGIN_DIR_DOVECOT mediante el siguiente comando:\n sieve_after = /var/lib/dovecot/sieve/spamfilter.sieve"  
$SUDO echo "require ["fileinto"];
# rule:[SPAM]
if header :contains "X-Spam-Level" "*" {
        fileinto "Junk";
}" >/var/lib/dovecot/sieve/spamfilter.sieve  
$SUDO sievec sieve/;
$SUDO chown -R vmail:vmail /var/lib/dovecot/sieve/*
$SUDO systemctl restart dovecot;
else 
       echo -e "Aun no ha instalado o tiene configurado el paquete dovecot-lmtpd, para ubuntu/debian ejecutar el comando \n
                sudo apt-get install dovecot-lmtpd";
    fi
}
######Instalar pyzor#######
$SUDO apt install pyzor
$SUDO pyzor --homedir /etc/mail/spamassassin/pyzor discover


